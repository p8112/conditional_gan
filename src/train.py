import yaml
from typing import Dict
from torchvision.transforms import (
    Compose, Resize, ToTensor, Normalize
)
from torchvision.datasets import ImageFolder
from torch.utils.data import DataLoader
from models import Discriminator, Generator
import torch.nn as nn
from torch.optim import Adam
from tqdm import tqdm
import torch
from torchvision.utils import save_image
from torch.utils.tensorboard import SummaryWriter


with open("config.yaml", mode="r") as file_obj:
    config: Dict = yaml.safe_load(file_obj)


train_transform = Compose(
    [
        Resize(size=(config["image_size"], config["image_size"])),
        ToTensor(),
        Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
    ]
)
train_dataset = ImageFolder(root=config["data_dir"], transform=train_transform)
train_dataloader = DataLoader(
    dataset=train_dataset, batch_size=config["batch_size"], shuffle=True
)


device = torch.device(config["device"])
generator = Generator(
    num_classes=config["num_classes"],
    embedding_dim=config["embedding_dim"],
    hidden_size=config["hidden_size"]
).to(device)
discriminator = Discriminator(
    num_classes=config["num_classes"],
    embedding_dim=config["embedding_dim"]
).to(device)
loss_func = nn.BCELoss().to(device)


generator_optimizer = Adam(
    params=generator.parameters(),
    lr=config["learning_rate"], betas=(config["beta_1"], config["beta_2"])
)
discriminator_optimizer = Adam(
    params=discriminator.parameters(),
    lr=config["learning_rate"], betas=(config["beta_1"], config["beta_2"])
)


eval_noise_vector = torch.randn(
    size=(config["num_eval_images"], config["hidden_size"])
).to(device)
eval_label = torch.randint(
    low=0, high=config["num_classes"], size=(config["num_eval_images"],),
    dtype=torch.long
).to(device)


writer = SummaryWriter(
    log_dir=f"{config['output_dir']}/log"
)


global_step: int = 0
for epoch in range(config["num_epochs"]):
    progress_bar = tqdm(
        iterable=enumerate(train_dataloader),
        desc=f"Epoch {epoch} training..."
    )

    for index, (real_image, label) in progress_bar:
        batch_size: int = real_image.shape[0]

        discriminator_optimizer.zero_grad()
        real_image = real_image.to(device)
        label = label.to(device)

        real_target = torch.ones(
            size=(batch_size,), dtype=torch.float
        ).to(device)
        discriminator_real_loss = loss_func(
            input=discriminator(image=real_image, label=label),
            target=real_target
        )

        noise_vector = torch.randn(
            size=(batch_size, config["hidden_size"])
        ).to(device)
        generated_image = generator(
            noise_vector=noise_vector, label=label
        )
        fake_target = torch.zeros(
            size=(batch_size,), dtype=torch.float
        ).to(device)
        discriminator_fake_loss = loss_func(
            input=discriminator(image=generated_image.detach(), label=label),
            target=fake_target
        )

        discriminator_total_loss = (discriminator_real_loss + discriminator_fake_loss) / 2
        discriminator_total_loss.backward()
        discriminator_optimizer.step()

        generator_optimizer.zero_grad()
        generator_loss = loss_func(
            input=discriminator(
                image=generated_image, label=label
            ),
            target=real_target
        )
        generator_loss.backward()
        generator_optimizer.step()

        writer.add_scalars(
            main_tag="discriminator_loss",
            tag_scalar_dict={
                "real_loss": discriminator_real_loss.item(),
                "fake_loss": discriminator_fake_loss.item()
            },
            global_step=global_step
        )
        writer.add_scalar(
            tag="generator_loss",
            scalar_value=generator_loss.item(),
            global_step=global_step
        )
        if global_step % config["num_steps_save_gen_image"] == 0:
            writer.add_images(
                tag="generator_image", img_tensor=generated_image.detach(),
                global_step=global_step
            )
        global_step += 1

    progress_bar.close()

    eval_image = generator(
        noise_vector=eval_noise_vector, label=eval_label
    )
    save_image(
        tensor=eval_image, fp=f"{config['output_dir']}/epoch_{epoch}_image.png",
        nrow=10, normalize=True
    )
    torch.save(
        obj=generator.state_dict(), f=f"{config['output_dir']}/epoch_{epoch}_generator.pt"
    )
    torch.save(
        discriminator.state_dict(), f=f"{config['output_dir']}/epoch_{epoch}_discriminator.pt"
    )
