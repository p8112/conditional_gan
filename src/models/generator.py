import torch.nn as nn
import torch
from torch import Tensor


class Generator(nn.Module):
    """
    Conditioned generator
    """
    def __init__(
            self, num_classes: int, embedding_dim: int, hidden_size: int
    ):
        """
        Init method
        :param num_classes: number of classes
        :param embedding_dim: embedding dim of classes
        :param hidden_size: hidden size (in latent space - z dim)
        """
        super(Generator, self).__init__()

        self.label_projector = nn.Sequential(
            nn.Embedding(
                num_embeddings=num_classes, embedding_dim=embedding_dim
            ),
            nn.Linear(
                in_features=embedding_dim, out_features=16
            )
        )

        self.latent_projector = nn.Sequential(
            nn.Linear(
                in_features=hidden_size, out_features=4 * 4 * 512
            ),
            nn.LeakyReLU(
                negative_slope=0.2, inplace=True
            )
        )

        self.model = nn.Sequential(
            nn.ConvTranspose2d(
                in_channels=513, out_channels=64 * 8,
                kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), bias=False
            ),
            nn.BatchNorm2d(
                num_features=64 * 8, momentum=0.1, eps=0.8
            ),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(
                in_channels=64 * 8, out_channels=64 * 4,
                kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), bias=False
            ),
            nn.BatchNorm2d(
                num_features=64 * 4, momentum=0.1, eps=0.8
            ),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(
                in_channels=64 * 4, out_channels=64 * 2,
                kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), bias=False
            ),
            nn.BatchNorm2d(
                num_features=64 * 2, momentum=0.1, eps=0.8
            ),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(
                in_channels=64 * 2, out_channels=64 * 1,
                kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), bias=False
            ),
            nn.BatchNorm2d(
                num_features=64 * 1, momentum=0.1, eps=0.8
            ),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(
                in_channels=64 * 1, out_channels=3,
                kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), bias=False
            ),
            nn.Tanh()
        )

        self.apply(fn=self.init_weight)

    @staticmethod
    def init_weight(model: nn.Module):
        """
        Init weight of model
        :param model: model weight
        :return:
        """
        if isinstance(model, nn.Conv2d):
            torch.nn.init.normal_(
                tensor=model.weight, mean=0.0, std=0.02
            )
        elif isinstance(model, nn.BatchNorm2d):
            torch.nn.init.normal_(
                tensor=model.weight, mean=1.0, std=0.02
            )
            torch.nn.init.zeros_(model.bias)

    def forward(self, noise_vector: Tensor, label: Tensor) -> Tensor:
        """
        Generate image
        :param noise_vector: batch size, hidden size
        :param label: batch size
        :return: batch size, 3, height, width
        """
        batch_size: int = noise_vector.shape[0]

        label_output = self.label_projector(label)   # batch size, 16
        label_output = label_output.reshape(batch_size, 1, 4, 4)    # batch size, 1, 4, 4

        latent_output = self.latent_projector(noise_vector)    # batch size, 4 * 4 * 512
        latent_output = latent_output.reshape(batch_size, 512, 4, 4)    # batch size, 512, 4, 4

        output = torch.cat(
            [latent_output, label_output], dim=1
        )    # batch size, 513, 4, 4
        output = self.model(output)    # batch size, 3, height, width

        return output
