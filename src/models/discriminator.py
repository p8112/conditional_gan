import torch.nn as nn
from torch import Tensor
import torch


class Discriminator(nn.Module):
    """
    Conditioned generator
    """
    def __init__(
            self, num_classes: int, embedding_dim: int
    ):
        """
        Init method
        :param num_classes: number of classes
        :param embedding_dim: embedding dim of class embedding layer
        """
        super(Discriminator, self).__init__()

        self.label_projector = nn.Sequential(
            nn.Embedding(
                num_embeddings=num_classes, embedding_dim=embedding_dim
            ),
            nn.Linear(
                in_features=embedding_dim, out_features=3 * 128 * 128
            )
        )

        self.model = nn.Sequential(
            nn.Conv2d(
                in_channels=6, out_channels=64,
                kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), bias=False
            ),
            nn.LeakyReLU(
                negative_slope=0.2, inplace=True
            ),
            nn.Conv2d(
                in_channels=64, out_channels=64 * 2,
                kernel_size=(4, 4), stride=(3, 3), padding=(2, 2), bias=False
            ),
            nn.BatchNorm2d(
                num_features=64 * 2, momentum=0.1, eps=0.8
            ),
            nn.LeakyReLU(
                negative_slope=0.2, inplace=True
            ),
            nn.Conv2d(
                in_channels=64 * 2, out_channels=64 * 4,
                kernel_size=(4, 4), stride=(3, 3), padding=(2, 2), bias=False
            ),
            nn.BatchNorm2d(
                num_features=64 * 4, momentum=0.1, eps=0.8
            ),
            nn.LeakyReLU(
                negative_slope=0.2, inplace=True
            ),
            nn.Conv2d(
                in_channels=64 * 4, out_channels=64 * 8,
                kernel_size=(4, 4), stride=(3, 3), padding=(2, 2), bias=False
            ),
            nn.BatchNorm2d(
                num_features=64 * 8, momentum=0.1, eps=0.8
            ),
            nn.LeakyReLU(
                negative_slope=0.2, inplace=True
            ),
            nn.Flatten(),
            nn.Dropout(p=0.4),
            nn.Linear(in_features=4608, out_features=1),
            nn.Sigmoid()
        )

        self.apply(fn=self.init_weight)

    @staticmethod
    def init_weight(model: nn.Module):
        """
        Init weight of model
        :param model: model weight
        :return:
        """
        if isinstance(model, nn.Conv2d):
            torch.nn.init.normal_(
                tensor=model.weight, mean=0.0, std=0.02
            )
        elif isinstance(model, nn.BatchNorm2d):
            torch.nn.init.normal_(
                tensor=model.weight, mean=1.0, std=0.02
            )
            torch.nn.init.zeros_(model.bias)

    def forward(self, image: Tensor, label: Tensor) -> Tensor:
        """
        Predict whether is fake or real
        :param image: batch size, 3, height, width
        :param label: batch size
        :return: batch size
        """
        batch_size: int = image.shape[0]

        label_output = self.label_projector(label)
        label_output = label_output.reshape(batch_size, 3, 128, 128)

        output = torch.cat([image, label_output], dim=1)
        output = self.model(output)
        output = output.squeeze(dim=1)

        return output
