import torch
from models import Generator
import yaml
from typing import Dict
import numpy as np
import matplotlib.pyplot as plt
import os
os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"


with open("config.yaml", mode="r") as file_obj:
    config: Dict = yaml.safe_load(file_obj)


device = torch.device(config["device"])
generator = Generator(
    num_classes=config["num_classes"],
    embedding_dim=config["embedding_dim"],
    hidden_size=config["hidden_size"]
).to(device)
generator.load_state_dict(
    torch.load(
        f"{config['output_dir']}/epoch_{config['epoch_to_eval']}_generator.pt",
        map_location=device
    ),
    strict=True
)
generator.eval()


points = torch.randn(
    size=(2, config["hidden_size"])
).to(device)
point_1 = points[0]
point_2 = points[1]


ratios = np.linspace(start=0, stop=1, num=10)
interpolated_points = []
for ratio in ratios:
    point = (1.0 - ratio) * point_1 + ratio * point_2
    interpolated_points.append(point)
interpolated_points = torch.stack(
    interpolated_points, dim=0
)


output = []
for class_idx in range(config["num_classes"]):
    label = class_idx * torch.ones(
        size=(10,), dtype=torch.long
    ).to(device)
    generated_image = generator(
        noise_vector=interpolated_points, label=label
    )
    generated_image = generated_image.permute(
        0, 2, 3, 1
    ).cpu().detach()
    output.append(generated_image)
output = torch.concat(output, dim=0)


fig: plt.Figure = plt.figure(
    figsize=(30, 9)
)
ax_index: int = 0
for i in range(config["num_classes"]):
    for j in range(10):
        ax: plt.Axes = fig.add_subplot(config["num_classes"], 10, ax_index + 1)
        tmp = (output[ax_index, :, :, :] + 1) * 127.5
        tmp = tmp.numpy()
        ax.imshow(tmp.astype(np.uint8))
        ax.axis('off')
        ax_index += 1


plt.show()
